{ mkDerivation, aeson, base, bytestring, Cabal, containers, dbus
, directory, elm, fetchElmDeps, filepath, happstack-server, hpack, HsYAML, MissingH
, process, scientific, stdenv, stm, text, unix, vector, vlc
}:
mkDerivation {
  pname = "piriot";
  version = "0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  buildDepends = [ elm ];
  preBuild = fetchElmDeps {
    elmPackages = import ./elm-config/elm-srcs.nix;
    elmVersion = "0.19.1";
    registryDat = ./elm-config/registry.dat;
  };
  postBuild = ''
    elm make ./src/client/elm/Main.elm --optimize --output=./src/client/www/elm.js
  '';
  postInstall = ''
    echo "postInstall"
    mkdir -p ./$name/
    mv $out/bin/piriot ./$name/piriot
    rmdir $out/bin/
    rm -rf $out/lib/
    mv ./src/client/www/ ./$name/
  '';
  postFixup = ''
    rm -rf $out/nix-support/
    mv ./src/server/install/install.sh ./$name/
    mv ./src/server/install/piriot.service ./$name/
    mv ./LICENSE.md ./$name/
    tar -czvf $out/$name.tar.gz $name/
  '';
  libraryHaskellDepends = [
    aeson base bytestring containers dbus directory filepath
    happstack-server HsYAML MissingH process scientific stm text unix
    vector
  ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    aeson base bytestring containers dbus directory filepath
    happstack-server HsYAML MissingH process scientific stm text unix
    vector
  ];
  prePatch = "hpack";
  license = stdenv.lib.licenses.agpl3Plus;
}
