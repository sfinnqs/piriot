let

  config = {
    packageOverrides = pkgs: rec {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = haskellPackagesNew: haskellPackagesOld: rec {
          piriot = haskellPackagesNew.callPackage ./piriot.nix {
            elm = pkgs.elmPackages.elm;
            fetchElmDeps = pkgs.elmPackages.fetchElmDeps;
            vlc = pkgs.vlc;
          };
        };
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

in
  { piriot = pkgs.haskellPackages.piriot;
  }
