module Piriot (Error (..), Fallible (..), fallibleToMaybe) where

import Data.Aeson (FromJSON (..), ToJSON (..), Value, eitherDecode, object, (.=))
import Data.Aeson.Types (Parser, parseEither)
import Data.Maybe
import Data.String
import Data.Text

data Error = Error Int Text (Maybe Value)

instance ToJSON Error where
  toJSON (Error c m d) = object ("code" .= c : "message" .= m : l)
    where
      l = ("data" .=) <$> maybeToList d

data Fallible a = Failure Error | Success a

fallibleToMaybe :: Fallible a -> Maybe a
fallibleToMaybe (Failure _) = Nothing
fallibleToMaybe (Success v) = Just v

instance Functor Fallible where
  fmap f (Failure e) = Failure e
  fmap f (Success x) = Success $ f x

instance Applicative Fallible where
  pure = Success
  (Failure e) <*> _ = Failure e
  (Success f) <*> x = f <$> x

instance Monad Fallible where
  (Failure e) >>= _ = Failure e
  (Success x) >>= f = f x

instance FromJSON a => FromJSON (Fallible a) where
  parseJSON v = return $ d $ parseEither parseJSON v
    where
      d (Left s) =
        Failure $ Error (-32600) "Invalid Request" $ Just $ fromString s
      d (Right r) = Success r
