module Piriot.Protocol.RPC
  ( Requests (..),
    Request (..),
    Params (..),
    Id (..),
    Responses (..),
    Response (..),
  )
where

import Data.Aeson
  ( Array,
    FromJSON (..),
    KeyValue,
    Object,
    ToJSON (..),
    Value (..),
    encode,
    object,
    parseJSON1,
    withObject,
    (.:),
    (.:!),
    (.=),
  )
import Data.Aeson.Encoding
import Data.Aeson.Types (Parser, unexpected)
import Data.List
import Data.Maybe
import Data.Scientific (Scientific)
import Data.Text
import qualified Data.Vector as V
import Piriot
import qualified Happstack.Server as H

data Requests = SingleRequest (Fallible Request) | BatchRequest [Fallible Request]

instance FromJSON Requests where
  parseJSON o@(Object _) = SingleRequest <$> parseJSON o
  parseJSON a@(Array v) =
    if V.null v
      then fail "An Array must have at least one value"
      else BatchRequest <$> parseJSON a
  parseJSON invalid = unexpected invalid

data Request = Request Text (Maybe Params) (Maybe Id)

instance FromJSON Request where
  parseJSON = withObject "RpcRequest" f
    where
      f v =
        Request
          <$> v .: "method"
          <*> v .:! "params"
          <*> v .:! "id"

data Params = ParamsArray Array | ParamsObject Object

instance FromJSON Params where
  parseJSON (Array a) = return $ ParamsArray a
  parseJSON (Object o) = return $ ParamsObject o
  parseJSON invalid = unexpected invalid

data Id = IdString Text | IdNumber Scientific | IdNull

instance ToJSON Id where
  toJSON (IdString s) = String s
  toJSON (IdNumber n) = Number n
  toJSON IdNull = Null

  toEncoding (IdString s) = text s
  toEncoding (IdNumber n) = scientific n
  toEncoding IdNull = null_

instance FromJSON Id where
  parseJSON (String t) = return $ IdString t
  parseJSON (Number s) = return $ IdNumber s
  parseJSON Null = return IdNull
  parseJSON invalid = unexpected invalid

data Responses = SingleResponse Response | BatchResponse [Response]

instance ToJSON Responses where
  toJSON (SingleResponse r) = toJSON r
  toJSON (BatchResponse r) = toJSON r
  toEncoding (SingleResponse r) = toEncoding r
  toEncoding (BatchResponse r) = toEncoding r

data Response = Response (Fallible Value) Id

instance ToJSON Response where
  toJSON (Response (Failure e) i) = object [jsonrpc, "error" .= e, "id" .= i]
  toJSON (Response (Success r) i) = object [jsonrpc, "result" .= r, "id" .= i]
  toEncoding (Response (Failure e) i) =
    pairs (jsonrpc <> "error" .= e <> "id" .= i)
  toEncoding (Response (Success r) i) =
    pairs (jsonrpc <> "result" .= r <> "id" .= i)

jsonrpc :: KeyValue kv => kv
jsonrpc = "jsonrpc" .= ("2.0" :: Text)

instance H.ToMessage Responses where
  toMessage = encode
  toContentType = const "application/json"
