module Piriot.Protocol.RPC.IO (handleRequests) where

import Control.Monad.IO.Class
import Data.Aeson (FromJSON, ToJSON, Value (..), eitherDecode)
import qualified Data.ByteString.Lazy as B
import Data.Maybe
import Data.String
import Data.Text
import qualified Data.Vector as V
import qualified Happstack.Server as H
import Piriot
import Piriot.Protocol.RPC
import Control.Monad

handleRequests :: (Request -> H.ServerPart (Fallible Value)) -> (Request -> H.ServerPart ()) -> H.ServerPart H.Response
handleRequests h nh = do
  H.setHeaderM "Access-Control-Allow-Origin" "*"
  H.setHeaderM "Allow" "OPTIONS, POST"
  msum [handleOptions, handlePost h nh]

handleOptions :: H.ServerPart H.Response
handleOptions = do
  H.method H.OPTIONS 
  H.setHeaderM "Access-Control-Allow-Headers" "content-type"
  H.setHeaderM "Access-Control-Allow-Methods" "OPTIONS, POST"
  return emptyResponse

handlePost :: (Request -> H.ServerPart (Fallible Value)) -> (Request -> H.ServerPart ()) -> H.ServerPart H.Response
handlePost h nh = do
  H.method H.POST
  r <- getRequests
  case r of
    Failure e -> resp $ Just $ SingleResponse $ Response (Failure e) IdNull
    Success requests -> handle requests
  where
    handle (SingleRequest r) = runFallible r >>= resp . fmap SingleResponse
    handle (BatchRequest r) =
      mapM runFallible r >>= resp . fmap BatchResponse . nonempty . catMaybes
    runFallible (Success r) = run r
    runFallible (Failure e) = return $ Just $ Response (Failure e) IdNull
    run r@(Request _ _ Nothing) = nh r >> return Nothing
    run r@(Request _ _ (Just i)) = do
      result <- h r
      return $ Just $ Response result i
    nonempty [] = Nothing
    nonempty xs = Just xs

getRequests :: H.ServerPart (Fallible Requests)
getRequests = t <$> getBody
  where
    t b = d $ eitherDecode b
    d (Left s) =
      Failure $ Error (-32600) "Invalid Request" $ Just $ fromString s
    d (Right r) = Success r

-- https://stackoverflow.com/a/8865950
getBody :: H.ServerPart B.ByteString
getBody = do
  req <- H.askRq
  maybe "" H.unBody <$> liftIO (H.takeRequestBody req)

resp :: Maybe Responses -> H.ServerPart H.Response
resp Nothing = H.setResponseCode 204 >> return emptyResponse
resp (Just r) = H.ok $ H.toResponse r

emptyResponse = H.toResponseBS "application/json" ""
