module Piriot.Protocol (handler) where

import Control.Concurrent.STM
import Control.Monad.IO.Class
import Data.Aeson (Value (..), object, (.=))
import Data.Text
import qualified Data.Vector as V
import qualified Happstack.Server as H
import Piriot
import Piriot.Config
import Piriot.Directory
import Piriot.Vlc
import Piriot.Protocol.RPC
import System.Process

data Action
  = Status
  | Start FilePath
  | Play
  | Pause

action :: Request -> Fallible Action
action (Request "status" _ _) = Success Status
action (Request "start" Nothing _) =
  Failure $ invalidParams "File must be specified"
action (Request "start" (Just (ParamsArray fs)) _)
  | V.null fs = Failure $ invalidParams "File must be specified"
  | otherwise = case V.head fs of
    (String f) -> Success $ Start $ unpack f
    _ -> Failure $ invalidParams "File must be a string"
action (Request "start" (Just _) _) =
  Failure $ invalidParams "Parameters must be a list"
action (Request "play" _ _) = Success Play
action (Request "pause" _ _) = Success Pause
action _ = Failure $ Error (-32601) "Method not found" Nothing

invalidParams :: Value -> Error
invalidParams d = Error (-32602) "Invalid params" $ Just d

handler :: Config -> TVar VlcStatus -> Request -> H.ServerPart (Fallible Value)
handler config vlcVar req = liftIO $ handleFallible $ action req
  where
    dc = downloads config
    handleFallible (Failure e) = return $ Failure e
    handleFallible (Success r) = handleAction r
    handleAction (Start f) = handleStart config vlcVar f
    handleAction Status = handleStatus dc vlcVar
    handleAction Play = handleUnpause vlcVar
    handleAction Pause = handlePause vlcVar

handleStatus :: DownloadsConfig -> TVar VlcStatus -> IO (Fallible Value)
handleStatus config vlcVar = do
  list <- getList config
  status <- getPlaybackStatus vlcVar
  return $ case (list, status) of
    (Failure e, _) -> Failure e
    (_, Failure e) -> Failure e
    (Success l, Success s) -> Success $ object ["videos" .= l, "status" .= s]
