module Piriot.Vlc
  ( VlcStatus,
    handleStart,
    getPlaybackStatus,
    handleUnpause,
    handlePause,
  )
where

import Control.Concurrent.STM
import Data.Aeson (ToJSON (..), Value (..))
import Data.List
import Data.List.Utils
import Data.Maybe
import Data.String
import Data.Text (unpack)
import Piriot
import Piriot.Config
import System.Directory
import System.FilePath
import System.IO
import System.Process
import Text.Read (readMaybe)

type VlcStatus = Maybe (Handle, Handle, ProcessHandle)

data PlaybackStatus = Stopped | Paused | Unpaused deriving (Eq)

instance Show PlaybackStatus where
  show Stopped = "stopped"
  show Paused = "paused"
  show Unpaused = "unpaused"

instance Read PlaybackStatus where
  readsPrec _ s =
    catMaybes $
      match
        <$> [ ("stopped", Stopped),
              ("paused", Paused),
              ("unpaused", Unpaused),
              ("playing", Unpaused)
            ]
    where
      match (text, status) = (,) status <$> stripPrefix text s

instance ToJSON PlaybackStatus where
  toJSON = String . fromString . show

handleStart :: Config -> TVar VlcStatus -> FilePath -> IO (Fallible Value)
handleStart conf procVar file = do
  result <- startVideo (vlc conf) fullFile
  cleanup procVar (fallibleToMaybe result)
  return $ Success $ Bool True
  where
    fullFile = directory (downloads conf) </> file

getPlaybackStatus :: TVar VlcStatus -> IO (Fallible PlaybackStatus)
getPlaybackStatus vlcVar = readTVarIO vlcVar >>= statusFromVlc

handleUnpause :: TVar VlcStatus -> IO (Fallible Value)
handleUnpause vlcVar = do
  vlcStatus <- readTVarIO vlcVar
  case vlcStatus of
    Nothing -> succeed
    Just (vlcIn, _, _) -> hPutStrLn vlcIn "play" >> succeed
  where
    succeed = return $ Success $ Bool True

handlePause :: TVar VlcStatus -> IO (Fallible Value)
handlePause vlcVar = do
  vlcStatus <- readTVarIO vlcVar
  case vlcStatus of
    Nothing -> succeed
    Just s@(vlcIn, _, _) -> do
      playback <- statusFromJust s
      case playback of
        Failure e -> return $ Failure e
        Success Unpaused -> hPutStrLn vlcIn "pause" >> succeed
        _ -> succeed
  where
    succeed = return $ Success $ Bool True

cleanup :: TVar VlcStatus -> VlcStatus -> IO ()
cleanup old new = do
  oldProc <- atomically $ swapTVar old new
  maybe (return ()) c oldProc
  where
    c (vlcIn, vlcOut, p) = do
      hPutStrLn vlcIn "shutdown"
      waitForProcess p
      cleanupProcess (Just vlcIn, Just vlcOut, Nothing, p)

startVideo :: FilePath -> FilePath -> IO (Fallible (Handle, Handle, ProcessHandle))
startVideo exe video = do
  executableExists <- doesFileExist exe
  fullPath <-
    if executableExists
      then return (Just exe)
      else findExecutable exe
  case fullPath of
    Nothing -> return $ Failure $ Error 501 "Unable to find executable file" $ Just $ String $ fromString exe
    Just f -> do
      (Just vlcIn, Just vlcOut, _, p) <- createProcess (proc f [video, "-I", "rc", "--rc-fake-tty", "--start-paused", "--play-and-exit"]) {std_in = CreatePipe, std_out = CreatePipe}
      hSetBuffering vlcIn LineBuffering
      hSetBuffering vlcOut LineBuffering
      return $ Success (vlcIn, vlcOut, p)

statusFromVlc :: VlcStatus -> IO (Fallible PlaybackStatus)
statusFromVlc Nothing = return $ Success Stopped
statusFromVlc (Just s) = statusFromJust s

statusFromJust :: (Handle, Handle, ProcessHandle) -> IO (Fallible PlaybackStatus)
statusFromJust (vlcIn, vlcOut, _) = hPutStrLn vlcIn "status" >> readStatus vlcOut

readStatus :: Handle -> IO (Fallible PlaybackStatus)
readStatus vlcOut = do
  readLine <- vlcGetLine vlcOut
  case readLine of
    Failure e -> return $ Failure e
    Success line ->
      case readStatusLine line of
        Nothing -> readStatus vlcOut
        Just s -> return $ Success s
  where
    readStatusLine line = case words line of
      ["(", "state", status, ")"] -> readMaybe status
      _ -> Nothing

vlcGetLine :: Handle -> IO (Fallible String)
vlcGetLine vlcOut = do
  ready <- hWaitForInput vlcOut 500
  if not ready
    then return $ Failure $ Error 500 "Unable to read from VLC" Nothing
    else Success . trimLine <$> hGetLine vlcOut
  where
    trimLine ('>' : ' ' : rest) = trimLine rest
    trimLine line = line
