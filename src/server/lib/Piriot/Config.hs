module Piriot.Config (Config (..), DownloadsConfig (..), defaultConfig, readConfig) where

import qualified Data.ByteString.Lazy as B
import Data.Maybe
import Data.String
import Data.Text (Text, unpack)
import Data.YAML
import System.Directory
import System.FilePath
import System.IO

data Config = Config
  { user :: Maybe String,
    port :: Int,
    webContent :: FilePath,
    downloads :: DownloadsConfig,
    vlc :: FilePath
  }
  deriving (Show, Eq)

data DownloadsConfig = DownloadsConfig
  {directory :: FilePath, videoExtensions :: [String]}
  deriving (Show, Eq)

defaultConfig :: IO Config
defaultConfig = do
  v <- fromMaybe "vlc" <$> findExecutable "vlc"
  dataDirs <- getXdgDirectoryList XdgDataDirs
  let pDataDirs = (</> "piriot") <$> dataDirs
  webDir <- bestDataDir pDataDirs "www"
  dlDir <- bestDataDir pDataDirs "downloads"
  return
    Config
      { user = Nothing,
        port = 80,
        webContent = webDir,
        downloads =
          DownloadsConfig
            { directory = dlDir,
              videoExtensions = defaultExtensions
            },
        vlc = v
      }

bestDataDir :: [FilePath] -> FilePath -> IO FilePath
bestDataDir dataDirs sub = bestPath $ (</> sub) <$> dataDirs

-- https://wiki.videolan.org/VLC_Features_Formats/#Format.2FContainer.2FMuxers
defaultExtensions :: [String]
defaultExtensions =
  [ "3gp",
    "a52",
    "aac",
    "asf",
    "au",
    "avi",
    "dts",
    "dv",
    "flac",
    "flv",
    "mp2",
    "mp3",
    "mpg",
    "mka",
    "mkv",
    "mov",
    "mp4",
    "nsc",
    "nsv",
    "nut",
    "ogg",
    "ogm",
    "ra",
    "ram",
    "rm",
    "rmbv",
    "rv",
    "tac",
    "ts",
    "tta",
    "ty",
    "vid",
    "wav",
    "wmv",
    "xa"
  ]

readConfig :: IO Config
readConfig = do
  configDirs <- getXdgDirectoryList XdgConfigDirs
  f <- bestPath $ (</> "piriot.yaml") <$> configDirs
  e <- doesFileExist f
  if e
    then do
      r <- B.readFile f
      putStrLn $ "Reading configuration from " ++ f
      case decode1 r of
        Left (p, err) -> do
          hPutStrLn stderr $ prettyPosWithSource p r err
          putStrLn "Using default configuration"
          defaultConfig
        Right config -> return config
    else do
      putStrLn $ "Creating default configuration at " ++ show f
      d <- defaultConfig
      B.writeFile f $ encode1 d
      return d

bestPath :: [FilePath] -> IO FilePath
bestPath paths = do
  exists <- findExists paths
  return $ fromMaybe (head paths) exists
  where
    findExists (p : ps) = do
      doesExist <- (||) <$> doesDirectoryExist p <*> doesFileExist p
      if doesExist then return (Just p) else findExists ps
    findExists [] = return Nothing

instance FromYAML Config where
  parseYAML =
    withMap "Config" f
    where
      f m =
        Config
          <$> maybeStringParser m "user"
          <*> m .: "port"
          <*> stringParser m "web content"
          <*> m .: "downloads"
          <*> stringParser m "vlc"

instance ToYAML Config where
  toYAML
    Config {user = u, port = p, webContent = w, downloads = dl, vlc = v} =
      mapping
        [ "user" .= (fromString <$> u :: (Maybe Text)),
          "port" .= p,
          "web content" .= (fromString w :: Text),
          "downloads" .= dl,
          "vlc" .= (fromString v :: Text)
        ]

instance FromYAML DownloadsConfig where
  parseYAML = withMap "DownloadsConfig" f
    where
      f m =
        DownloadsConfig
          <$> stringParser m "directory"
          <*> stringListParser m "video extensions"

instance ToYAML DownloadsConfig where
  toYAML DownloadsConfig {directory = d, videoExtensions = v} =
    mapping
      [ "directory" .= (fromString d :: Text),
        "video extensions" .= (fromString <$> v :: [Text])
      ]

stringParser :: Mapping Pos -> Text -> Parser String
stringParser m k = unpack <$> (m .: k :: Parser Text)

maybeStringParser :: Mapping Pos -> Text -> Parser (Maybe String)
maybeStringParser m k = fmap unpack <$> (m .:? k :: Parser (Maybe Text))

stringListParser :: Mapping Pos -> Text -> Parser [String]
stringListParser m k = fmap unpack <$> (m .: k :: Parser [Text])
