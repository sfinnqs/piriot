module Piriot.Directory (getList) where

import Data.Aeson (ToJSON (..), Value (..), object, (.=))
import Data.Aeson.Encoding
import Data.Maybe
import Data.String
import Piriot
import Piriot.Config
import System.Directory
import System.FilePath

data FileOrDirectory = File FilePath | Directory FilePath [FileOrDirectory]

instance ToJSON FileOrDirectory where
  toJSON (File f) = String (fromString f)
  toJSON (Directory n d) = object ["name" .= n, "contents" .= toJSONList d]
  toEncoding (File f) = string f
  toEncoding (Directory n d) = pairs ("name" .= n <> "contents" .= d)

getList :: DownloadsConfig -> IO (Fallible Value)
getList config =
  Success . toJSONList
    <$> listFiles (isVideo (videoExtensions config)) (directory config)

-- TODO handle failure
listFiles :: (FilePath -> Bool) -> FilePath -> IO [FileOrDirectory]
listFiles p directory = do
  isDirectory <- doesDirectoryExist directory
  if isDirectory
    then do
      list <- listDirectory directory
      catMaybes <$> mapM (listHelper p directory) list
    else return []

listHelper :: (FilePath -> Bool) -> FilePath -> FilePath -> IO (Maybe FileOrDirectory)
listHelper p cwd path = do
  isFile <- doesFileExist fullPath
  if isFile
    then return $ if p path then Just (File path) else Nothing
    else do
      list <- listDirectory fullPath
      subPaths <- catMaybes <$> mapM (listHelper p fullPath) list
      return $ case subPaths of
        [] -> Nothing
        _ : _ -> Just $ Directory path subPaths
  where
    fullPath = cwd </> path

isVideo :: [String] -> FilePath -> Bool
isVideo exts file = any (`isExtensionOf` file) exts
