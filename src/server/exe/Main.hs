module Main where

import Control.Applicative
import Control.Concurrent.STM
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.ByteString.Lazy as B
import Data.ByteString.Lazy.Char8 (unpack)
import Data.List.Utils
import Data.Map.Strict (Map)
import Data.String
import Data.Text (unpack)
import qualified Happstack.Server as H
import Piriot.Config
import Piriot.Protocol
import Piriot.Protocol.RPC.IO
import Piriot.Vlc
import System.Directory
import System.Environment
import System.FilePath
import System.IO
import System.Posix.User
import System.Process

main :: IO ()
main = do
  conf <- readConfig
  let serverConf = H.nullConf {H.port = port conf}
  socket <- H.bindPort serverConf
  case user conf of
    Nothing -> return ()
    Just u -> getUserEntryForName u >>= setUserID . userID
  vlcVar <- liftIO $ newTVarIO Nothing
  H.simpleHTTPWithSocket socket serverConf $
    H.dir "api" (api conf vlcVar) <|> web (webContent conf)

api :: Config -> TVar VlcStatus -> H.ServerPart H.Response
api conf vlcVar = do
  handleRequests (handler conf vlcVar) (const mzero)

web :: FilePath -> H.ServerPart H.Response
web = H.serveDirectory H.EnableBrowsing ["index.html"]
