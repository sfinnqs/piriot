#!/bin/bash
echo "Installing piriot to /usr/local/bin/"
install piriot /usr/local/bin/
data1=${XDG_DATA_DIRS%%:*} # first entry (before ':')
data=${data1:-/usr/local/share/} # with default
content=${data%/}/piriot/ # resolve
echo "Copying web content to ${content}www"
cp -r www/ "$content"
echo "Copying piriot.service to /etc/systemd/system/"
install piriot.service /etc/systemd/system/
echo "Enabling piriot.service"
systemctl enable piriot --now
