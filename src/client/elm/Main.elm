module Main exposing (Model, Msg(..), init, main, subscriptions, update, view)

import Bootstrap.Button as Button
import Bootstrap.ListGroup as ListGroup
import Bootstrap.Modal as Modal
import Browser exposing (Document)
import Browser.Navigation as Nav
import Html exposing (Attribute, Html, a, div, h1, input, label, li, p, span, strong, text, ul)
import Html.Attributes exposing (attribute, class, for, href, id, type_)
import Html.Events exposing (onClick, onInput)
import Http exposing (Expect, expectJson, jsonBody)
import Json.Decode as Dec exposing (Decoder, andThen, bool, fail, field, map2, maybe, oneOf, succeed, value)
import Json.Encode as Enc exposing (Value, encode, object)
import List exposing (concat, intersperse, map, unzip)
import Maybe exposing (withDefault)
import String exposing (append, dropLeft, fromInt, indexes, left, split)
import Svg exposing (Svg, path, svg)
import Svg.Attributes as SvgA exposing (d, fill, fillRule, viewBox)
import Tuple exposing (pair)
import Url



-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type alias Model =
    { key : Nav.Key
    , error : Maybe ErrorMessage
    , search : SearchData
    , remote : RemoteData
    , rpcId : Int
    }


getTab : Model -> Tab
getTab m =
    case m.remote of
        NotPlaying ->
            Search

        Playing d ->
            if d.isShown then
                Remote

            else
                Search


type alias ErrorMessage =
    { title : String, message : String }


type Tab
    = Search
    | Remote


type alias SearchData =
    { query : String, videos : List FileOrDirectory, selected : Maybe String }


type FileOrDirectory
    = File String
    | Directory String (List FileOrDirectory)


filename : FileOrDirectory -> String
filename f =
    case f of
        File n ->
            n

        Directory n _ ->
            n


type RemoteData
    = NotPlaying
    | Playing { isShown : Bool, isPaused : Bool }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ _ key =
    ( Model key Nothing (SearchData "" [] Nothing) NotPlaying 0
    , Cmd.batch [ Nav.replaceUrl key "/", performAction Status ]
    )



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | HttpError Http.Error
    | ServerError String
    | ServerStatusUpdated PlaybackStatus (List FileOrDirectory)
    | ServerVideoStarted
    | ServerVideoResumed
    | ServerVideoPaused
    | TabChanged Tab
    | Queried String
    | VideoSelected String
    | FolderSelected (Maybe String)
    | VideoPlayed
    | VideoPaused


type PlaybackStatus
    = Unpaused
    | Paused
    | Stopped


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        search =
            model.search

        remote =
            model.remote
    in
    case msg of
        LinkClicked request ->
            ( model, clickLink request )

        UrlChanged _ ->
            ( model, Cmd.none )

        HttpError e ->
            ( { model | error = Just <| errorMessage e }, Cmd.none )

        ServerError e ->
            ( { model | error = Just <| ErrorMessage "Server error" e }
            , Cmd.none
            )

        ServerStatusUpdated status l ->
            ( { model
                | error = Nothing
                , search = { search | videos = l }
                , remote = statusUpdateRemote status remote
              }
            , Cmd.none
            )

        ServerVideoStarted ->
            ( { model | error = Nothing, remote = startVideoPlay }, Cmd.none )

        ServerVideoResumed ->
            ( { model | error = Nothing, remote = pauseVideoPlay False remote }, Cmd.none )

        ServerVideoPaused ->
            ( { model | error = Nothing, remote = pauseVideoPlay True remote }, Cmd.none )

        TabChanged tab ->
            ( { model
                | remote = changeTabPlay tab model.remote
                , search = { search | selected = Nothing }
              }
            , Cmd.none
            )

        Queried query ->
            ( { model | search = { search | query = query } }, Cmd.none )

        FolderSelected video ->
            ( { model | search = { search | selected = video } }, Cmd.none )

        VideoSelected video ->
            ( model, performAction <| Start video )

        VideoPlayed ->
            ( model, performAction Play )

        VideoPaused ->
            ( model, performAction Pause )


errorMessage : Http.Error -> ErrorMessage
errorMessage e =
    case e of
        Http.BadUrl url ->
            ErrorMessage "Bad URL" url

        Http.Timeout ->
            ErrorMessage "Timeout" "Server took too long to respond"

        Http.NetworkError ->
            ErrorMessage "Network error" "Check your internet connection"

        Http.BadStatus status ->
            ErrorMessage "Bad status" <| "The server returned" ++ fromInt status

        Http.BadBody s ->
            ErrorMessage "Unable to parse server response" s


startVideoPlay : RemoteData
startVideoPlay =
    Playing { isShown = True, isPaused = True }


pauseVideoPlay : Bool -> RemoteData -> RemoteData
pauseVideoPlay pause p =
    case p of
        NotPlaying ->
            Playing { isShown = False, isPaused = pause }

        Playing d ->
            Playing { d | isPaused = pause }


changeTabPlay : Tab -> RemoteData -> RemoteData
changeTabPlay tab play =
    case ( play, tab ) of
        ( NotPlaying, _ ) ->
            NotPlaying

        ( Playing d, Search ) ->
            Playing { d | isShown = False }

        ( Playing d, Remote ) ->
            Playing { d | isShown = True }


statusUpdateRemote : PlaybackStatus -> RemoteData -> RemoteData
statusUpdateRemote status remote =
    case ( status, remote ) of
        ( Paused, Playing d ) ->
            Playing { d | isPaused = True }

        ( Paused, NotPlaying ) ->
            Playing { isShown = True, isPaused = True }

        ( Unpaused, Playing d ) ->
            Playing { d | isPaused = False }

        ( Unpaused, NotPlaying ) ->
            Playing { isShown = True, isPaused = False }

        ( Stopped, _ ) ->
            NotPlaying


clickLink : Browser.UrlRequest -> Cmd Msg
clickLink url =
    case url of
        Browser.Internal _ ->
            Cmd.none

        Browser.External u ->
            Nav.load u



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    let
        ( searchStatus, playStatus ) =
            case model.remote of
                NotPlaying ->
                    ( Active, Disabled )

                Playing d ->
                    if d.isShown then
                        ( Enabled, Active )

                    else
                        ( Active, Enabled )
    in
    Document
        "Piriot"
        [ div
            [ class "container" ]
            [ viewError model.error
            , h1 [ class "display-1" ] [ text "Piriot" ]
            , ul
                [ class "nav nav-pills" ]
                [ viewPill searchStatus Search, viewPill playStatus Remote ]
            , viewContent model
            ]
        ]


viewError : Maybe ErrorMessage -> Html Msg
viewError error =
    case error of
        Nothing ->
            text ""

        Just e ->
            div
                [ class "alert alert-danger", role "alert" ]
                [ strong [] [ text <| e.title ++ ": " ], text e.message ]


role : String -> Attribute Msg
role =
    attribute "role"


type PillStatus
    = Disabled
    | Active
    | Enabled


viewPill : PillStatus -> Tab -> Html Msg
viewPill status tab =
    let
        staticAttributes =
            [ class c, href "#" ]

        attributes =
            case status of
                Enabled ->
                    (onClick <| TabChanged tab) :: staticAttributes

                _ ->
                    staticAttributes

        c =
            case status of
                Disabled ->
                    "nav-link disabled"

                Active ->
                    "nav-link active"

                Enabled ->
                    "nav-link"
    in
    li [ class "nav-item" ] [ a attributes [ text <| tabName tab ] ]


tabName : Tab -> String
tabName tab =
    case tab of
        Search ->
            "Search"

        Remote ->
            "Play"


viewContent : Model -> Html Msg
viewContent model =
    let
        search =
            viewSearch model.search
    in
    case model.remote of
        Playing d ->
            if d.isShown then
                viewRemote d.isPaused

            else
                search

        NotPlaying ->
            search


viewSearch : SearchData -> Html Msg
viewSearch data =
    let
        ( buttons, modals ) =
            contentsList data.selected Nothing data.videos
    in
    div [] <|
        div
            [ class "form-group" ]
            [ label [ for "q" ] [ text "Search" ]
            , input
                [ type_ "text", class "form-control", id "q", onInput Queried ]
                []
            ]
            :: buttons
            :: modals


viewVideoButton : Maybe String -> Maybe String -> FileOrDirectory -> ( ListGroup.CustomItem Msg, List (Html Msg) )
viewVideoButton activated container item =
    let
        fullName =
            case container of
                Nothing ->
                    filename item

                Just c ->
                    c ++ "/" ++ filename item

        click =
            case item of
                File _ ->
                    VideoSelected fullName

                Directory n _ ->
                    FolderSelected <|
                        if Just n == activated then
                            Nothing

                        else
                            Just fullName

        content =
            case item of
                File f ->
                    iconPlay :: viewVideoFilename f

                Directory n f ->
                    viewVideoFilename n ++ [ iconExpand ]

        modals =
            case item of
                File f ->
                    []

                Directory n f ->
                    viewFolderModal activated fullName container f
    in
    ( ListGroup.button [ ListGroup.attrs [ onClick click ] ] content
    , modals
    )


viewFolderModal : Maybe String -> String -> Maybe String -> List FileOrDirectory -> List (Html Msg)
viewFolderModal activated name prev subPaths =
    let
        visibility =
            if Just name == activated then
                Modal.shown

            else
                Modal.hidden

        ( buttons, modals ) =
            contentsList activated (Just name) subPaths
    in
    (Modal.config (FolderSelected Nothing)
        |> Modal.small
        |> Modal.hideOnBackdropClick True
        |> Modal.h5 [] (viewVideoFilename name)
        |> Modal.body [] [ buttons ]
        |> Modal.footer []
            [ Button.button
                [ Button.primary
                , Button.attrs [ onClick (FolderSelected prev) ]
                ]
                [ iconBack, text "Back" ]
            , Button.button
                [ Button.secondary
                , Button.attrs [ onClick (FolderSelected Nothing) ]
                ]
                [ text "Close" ]
            ]
        |> Modal.view visibility
    )
        :: modals


viewVideoFilename : String -> List (Html Msg)
viewVideoFilename =
    splitLines
        [ ( splitBeforeExtension, False )
        , ( splitBefore " ", True )

        --[ splitBefore " "
        , ( splitBefore "-", False )
        , ( splitBefore ".", False )
        ]



mapExceptFirst : (a -> a) -> List a -> List a
mapExceptFirst f l =
    case l of
        [] ->
            []

        x :: xs ->
            x :: map f xs


splitLines : List ( String -> List String, Bool ) -> String -> List (Html Msg)
splitLines splitters s =
    case splitters of
        [] ->
            [ text s ]

        ( splitter, i ) :: next ->
            let
                result =
                    map (breakable << splitLines next) <| splitter s
            in
            if i then
                intersperse (text " ") result

            else
                result


splitBefore : String -> String -> List String
splitBefore sep =
    mapExceptFirst (append sep) << split sep


splitBeforeExtension : String -> List String
splitBeforeExtension string =
    let
        index =
            last <| indexes "." string
    in
    case index of
        Nothing ->
            [ string ]

        Just i ->
            [ left i string, dropLeft i string ]


last : List a -> Maybe a
last l =
    case l of
        [] ->
            Nothing

        x :: xs ->
            Just <| withDefault x <| last xs


breakable : List (Html Msg) -> Html Msg
breakable e =
    span [ class "line" ] e


contentsList : Maybe String -> Maybe String -> List FileOrDirectory -> ( Html Msg, List (Html Msg) )
contentsList activated container files =
    let
        ( listItems, modals ) =
            unzip <| map (viewVideoButton activated <| container) files
    in
    ( ListGroup.custom <| listItems, concat modals )


viewRemote : Bool -> Html Msg
viewRemote isPaused =
    let
        icon =
            if isPaused then
                largeIconPlay

            else
                largeIconPause

        click =
            if isPaused then
                VideoPlayed

            else
                VideoPaused

        attributes =
            if isPaused then
                [ Button.outlinePrimary, Button.block, Button.large, Button.attrs [ onClick click ] ]

            else
                [ Button.primary, Button.block, Button.large, Button.attrs [ onClick click ] ]
    in
    Button.button attributes [ icon ]



-- ICONS


bootstrapIcon : List (Svg Msg) -> Html Msg
bootstrapIcon =
    svg
        [ SvgA.width "1em"
        , SvgA.height "1em"
        , viewBox "0 0 16 16"
        , fill "currentColor"
        ]


largeBootstrapIcon : List (Svg Msg) -> Html Msg
largeBootstrapIcon =
    svg [ viewBox "0 0 16 16", fill "currentColor" ]


iconPlay : Html Msg
iconPlay =
    bootstrapIcon [ playPath ]


largeIconPlay : Html Msg
largeIconPlay =
    largeBootstrapIcon [ playPath ]


playPath : Svg Msg
playPath =
    path
        [ d "M11.596 8.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z"
        ]
        []


largeIconPause : Html Msg
largeIconPause =
    largeBootstrapIcon
        [ path
            [ d "M5.5 3.5A1.5 1.5 0 0 1 7 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5zm5 0A1.5 1.5 0 0 1 12 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5z"
            ]
            []
        ]


iconExpand : Html Msg
iconExpand =
    span
        [ class "float-right" ]
        [ bootstrapIcon
            [ path
                [ fillRule "evenodd"
                , d "M8.146 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.793 8 8.146 5.354a.5.5 0 0 1 0-.708z"
                ]
                []
            , path
                [ fillRule "evenodd"
                , d "M4 8a.5.5 0 0 1 .5-.5H11a.5.5 0 0 1 0 1H4.5A.5.5 0 0 1 4 8z"
                ]
                []
            ]
        ]


iconBack : Html Msg
iconBack =
    bootstrapIcon
        [ path
            [ fillRule "evenodd"
            , d "M7.854 4.646a.5.5 0 0 1 0 .708L5.207 8l2.647 2.646a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"
            ]
            []
        , path
            [ fillRule "evenodd"
            , d "M4.5 8a.5.5 0 0 1 .5-.5h6.5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"
            ]
            []
        ]


iconSize : Int
iconSize =
    24



-- HTTP


type Action
    = Status
    | Start String
    | Play
    | Pause


encodeAction : Action -> Value
encodeAction action =
    let
        ( method, params ) =
            case action of
                Status ->
                    ( "status", Nothing )

                Start file ->
                    ( "start", Just <| Enc.list Enc.string [ file ] )

                Play ->
                    ( "play", Nothing )

                Pause ->
                    ( "pause", Nothing )

        pairs =
            [ ( "jsonrpc", Enc.string "2.0" )
            , ( "method", Enc.string method )
            , ( "id", Enc.null )
            ]

        paramPairs =
            case params of
                Nothing ->
                    pairs

                Just p ->
                    ( "params", p ) :: pairs
    in
    object paramPairs


performAction : Action -> Cmd Msg
performAction action =
    Http.post
        { url = "/api"
        , body = jsonBody <| encodeAction action
        , expect = expect action
        }


expect : Action -> Expect Msg
expect action =
    case action of
        Status ->
            expectHelper (toMsg2 ServerStatusUpdated) <|
                Dec.map2 pair (field "status" decodeStatus) <|
                    field "videos" <|
                        Dec.list decodeFileOrDirectory

        Start _ ->
            expectHelper (toMsg0 ServerVideoStarted) <| bool

        Play ->
            expectHelper (toMsg0 ServerVideoResumed) <| bool

        Pause ->
            expectHelper (toMsg0 ServerVideoPaused) <| bool


type alias ServerResult a =
    Result Http.Error (Result String a)


type alias ServerResult2 a b =
    Result Http.Error (Result String ( a, b ))


expectHelper : (ServerResult a -> Msg) -> Decoder a -> Expect Msg
expectHelper toMsg decoder =
    expectJson toMsg <| decodeResult decoder


toMsg0 : Msg -> ServerResult a -> Msg
toMsg0 msg result =
    case result of
        Ok (Ok _) ->
            msg

        Ok (Err e) ->
            ServerError e

        Err e ->
            HttpError e


toMsg1 : (a -> Msg) -> ServerResult a -> Msg
toMsg1 constructor result =
    case result of
        Ok (Ok v) ->
            constructor v

        Ok (Err e) ->
            ServerError e

        Err e ->
            HttpError e


toMsg2 : (a -> b -> Msg) -> ServerResult2 a b -> Msg
toMsg2 constructor result =
    case result of
        Ok (Ok ( v1, v2 )) ->
            constructor v1 v2

        Ok (Err e) ->
            ServerError e

        Err e ->
            HttpError e


decodeFileOrDirectory : Decoder FileOrDirectory
decodeFileOrDirectory =
    oneOf [ decodeFile, decodeDirectory ]


decodeFile : Decoder FileOrDirectory
decodeFile =
    Dec.map File Dec.string


decodeDirectory : Decoder FileOrDirectory
decodeDirectory =
    Dec.map2 Directory (field "name" Dec.string) <|
        field "contents" <|
            Dec.list <|
                Dec.lazy (\_ -> decodeFileOrDirectory)


decodeStatus : Decoder PlaybackStatus
decodeStatus =
    Dec.string |> andThen parseStatus


parseStatus : String -> Decoder PlaybackStatus
parseStatus s =
    case s of
        "unpaused" ->
            succeed Unpaused

        "paused" ->
            succeed Paused

        "stopped" ->
            succeed Stopped

        _ ->
            fail ("Unable to parse playback status: " ++ s)


decodeResult : Decoder a -> Decoder (Result String a)
decodeResult d =
    oneOf [ Dec.map Ok <| field "result" d, decodeError ]


decodeError : Decoder (Result String a)
decodeError =
    Dec.map Err
        (field "error" <|
            (map2 errorFromResponse (field "message" Dec.string) <|
                maybe (field "data" value)
            )
        )


errorFromResponse : String -> Maybe Value -> String
errorFromResponse message data =
    case data of
        Nothing ->
            message

        Just d ->
            message ++ ": " ++ encode 0 d


type alias Response =
    Result Error Value


type alias Error =
    { code : Int, message : String, data : Value }
